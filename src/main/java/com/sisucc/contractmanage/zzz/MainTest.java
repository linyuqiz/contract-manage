package com.sisucc.contractmanage.zzz;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.core.lang.Console;
import cn.hutool.core.util.StrUtil;
import com.sisucc.contractmanage.constant.PropertyString;
import com.sisucc.contractmanage.util.LocalDateTimeUtil;
import com.sisucc.contractmanage.util.PropertyUtil;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author linyuqi
 */
public class MainTest {

    List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
    List<Integer> list1 = Arrays.asList(6, 7, 8, 9, 10);

    @Test
    public void test1() {
        List<Integer> collect = Stream.of(list, list1).flatMap(Collection::stream).collect(Collectors.toList());
        collect.forEach(System.out::println);

    }

    @Test
    public void test2() throws ParseException {
        String str = "Thu Jan 21 2021 22:50:30 GMT+0800 (中国标准时间)";
        System.out.println(LocalDateTimeUtil.GMTToLocalDateTime(str));
    }

    @Test
    public void test3() {
        Optional.of(list).ifPresent(System.out::println);
    }

    @Test
    public void test4() {
        String str = "";
        System.out.println(StrUtil.isNotBlank(str));
        System.out.println(StrUtil.isNotEmpty(str));
    }

    @Test
    public void test5() {
        String str = "Fri Jan 01 2021 23:07:00 GMT+0800 (中国标准时间)";
        System.out.println(str.substring(0, 15) + PropertyString.START_TIME_SUFFIX);
        System.out.println(str.substring(0, 15) + PropertyString.END_TIME_SUFFIX);
    }

    @Test
    public void test6() {
        LocalDateTime localDateTime = null;
        System.out.println(Optional.ofNullable(localDateTime).isPresent());
    }

    @Test
    public void test7() {
        //获取秒数
        Long second = LocalDateTime.now().toEpochSecond(ZoneOffset.of("+8"));
        //获取毫秒数
        Long milliSecond = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();
        System.out.println(second);
        System.out.println(milliSecond);
    }

    @Test
    public void test8() {
        for (int i = 0; i < 100; i++) {
            System.out.println(PropertyUtil.getUUID());
        }
    }

    @Test
    public void test9() {
        //定义图形验证码的长和宽
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(200, 100);

        //图形验证码写出，可以写出到文件，也可以写出到流
                lineCaptcha.write("d:/line.png");
        //输出code
                Console.log(lineCaptcha.getCode());
        //验证图形验证码的有效性，返回boolean值
                lineCaptcha.verify("1234");

        //重新生成验证码
                lineCaptcha.createCode();
                lineCaptcha.write("d:/line.png");
        //新的验证码
                Console.log(lineCaptcha.getCode());
        //验证图形验证码的有效性，返回boolean值
        lineCaptcha.verify("1234");

    }

    @Test
    public void test10() {
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(200, 100);
        System.out.println(lineCaptcha.getImageBase64Data());
    }

    @Test
    public void test11() {
        System.out.println(UUID.randomUUID());
    }

    @Test
    public void test12() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");

        Date date = new Date();

        Calendar c = Calendar.getInstance();

        c.setTime(date);

        c.add(Calendar.DATE, 15);

        System.out.println(sdf.format(c.getTime()));
    }

    @Test
    public void test13() {
//        System.out.println(LocalDateTimeUtil.getLocalDateTimeStartByFlagTime(0));
//        System.out.println(LocalDateTimeUtil.getLocalDateTimeEndByFlagTime(0));

        System.out.println(LocalDateTime.now().toString());

    }

}
