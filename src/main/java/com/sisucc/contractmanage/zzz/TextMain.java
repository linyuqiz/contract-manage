package com.sisucc.contractmanage.zzz;

import com.spire.doc.Document;

import java.io.FileWriter;
import java.io.IOException;

/**
 * @author linyuqi
 */
public class TextMain {
    public static void main(String[] args) throws IOException {
        //加载测试文档
        Document doc = new Document();
        doc.loadFromFile("/Users/linyuqi/Desktop/李浩.开题报告.doc");

        //获取文本保存为String
        String text = doc.getText();

        System.out.println(text);
    }
}
