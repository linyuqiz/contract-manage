package com.sisucc.contractmanage.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * @author linyuqi
 */
public class LocalDateTimeUtil {

    /**
     * GMT时间转LocalDateTime
     *
     * @param dateString
     * @return
     * @throws ParseException
     */
    public static LocalDateTime GMTToLocalDateTime(String dateString) throws ParseException {
        String pattern = "EEE MMM dd yyyy HH:mm:ss 'GMT'";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.ENGLISH);
        Instant instant = sdf.parse(dateString).toInstant();
        return instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    /**
     * 获取n天的最初时间
     * @param flagTime 天数
     * @return
     */
    public static LocalDateTime getLocalDateTimeStartByFlagTime(int flagTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, flagTime);
        return LocalDateTime.of(calendar.get(Calendar.YEAR), (calendar.get(Calendar.MONTH) + 1), calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
    }

    /**
     * 获取n天的最后时间
     * @param flagTime 天数
     * @return
     */
    public static LocalDateTime getLocalDateTimeEndByFlagTime(int flagTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, flagTime);
        return LocalDateTime.of(calendar.get(Calendar.YEAR), (calendar.get(Calendar.MONTH) + 1), calendar.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
    }

    public static String getStringByLocalDateTime(LocalDateTime localDateTime) {
//        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//        String localTime = df.format(localDateTime);
//        return localTime;
        return localDateTime.toString();
    }

}
