package com.sisucc.contractmanage.util;

import com.sisucc.contractmanage.constant.PropertyString;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Random;

/**
 * 生成所需要的属性
 *
 * @author linyuqi
 */
public class PropertyUtil {

    /**
     * 生成UUID
     *
     * @return
     */
    public static String getUUID() {
        int rand = new Random().nextInt(900000) + 100000;
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter form = DateTimeFormatter.ofPattern("yyyyMMdd");
        String dateString = now.format(form);
        Long second = LocalDateTime.now().toEpochSecond(ZoneOffset.of("+0"));
        return dateString + second.toString().substring(0, 4) + rand;
    }

    /**
     * 生成当前时间
     *
     * @return
     */
    public static LocalDateTime getCurrentDateTime() {
        return LocalDateTime.now();
    }

    /**
     * 生成开始时间
     *
     * @return
     */
    public static String getStartTimeString(String startTime) {
        return startTime.substring(0, 15) + PropertyString.START_TIME_SUFFIX;
    }

    /**
     * 生成结束时间
     *
     * @return
     */
    public static String getEndTimeString(String endTime) {
        return endTime.substring(0, 15) + PropertyString.END_TIME_SUFFIX;
    }
}
