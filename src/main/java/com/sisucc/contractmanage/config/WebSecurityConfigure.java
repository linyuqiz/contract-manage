package com.sisucc.contractmanage.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author linyuqi
 */
@Configuration
public class WebSecurityConfigure extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin()
                .loginPage("/loginPage")
                .loginProcessingUrl("/login")
                .successForwardUrl("/toIndex")
                .failureForwardUrl("/toLogin")
                .and()
                .logout()
                .logoutUrl("/logout")
                .and()
                .authorizeRequests()
                .antMatchers("/loginPage").permitAll()
                .antMatchers("/toLogin").permitAll()
                .antMatchers("/validate/*").permitAll()
                .antMatchers("/library/*").permitAll()
                .antMatchers("/pages/*").permitAll()
                .antMatchers("/constant/*").permitAll()
                .anyRequest().authenticated()
                .and()
                .csrf();

        http.sessionManagement().maximumSessions(1).expiredUrl("/toLogin");
    }

    @Bean
    public PasswordEncoder getPassWord() {
        return new BCryptPasswordEncoder();
    }
}
