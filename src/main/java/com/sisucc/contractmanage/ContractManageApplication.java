package com.sisucc.contractmanage;

import io.github.bluemiaomiao.annotation.EnableFastdfsClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.oas.annotations.EnableOpenApi;

/**
 * swagger url:http://localhost:8080/swagger-ui/index.html
 * swagger bootstrap url:http://localhost:8080/doc.html
 * @author linyuqi
 */
@EnableAsync
@EnableOpenApi
@EnableScheduling
@EnableFastdfsClient
@SpringBootApplication
public class ContractManageApplication {

    public static void main(String[] args) {
        SpringApplication.run(ContractManageApplication.class, args);
    }
}

