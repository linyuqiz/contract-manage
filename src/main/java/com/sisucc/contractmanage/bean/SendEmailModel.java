package com.sisucc.contractmanage.bean;

/**
 * 邮件发送参数封装
 * @author linyuqi
 */
public class SendEmailModel {

    /**
     * 收件人邮箱
     */
    private String receiver ;


    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

}
