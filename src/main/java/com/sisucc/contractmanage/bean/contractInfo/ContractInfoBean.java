package com.sisucc.contractmanage.bean.contractInfo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author linyuqi
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tb_contract_info")
public class ContractInfoBean {

    @Id
    @Column(name = "contract_id",length = 50, nullable = false)
    private String contractId;

    @Column(name = "contract_name", length = 50)
    private String contractName;

    @Column(name = "contract_status", length = 50)
    private String contractStatus;

    @Column(name = "staff_code", length = 50)
    private String staffCode;

    @Column(name = "account_code", length = 50)
    private String accountCode;

    @Column(name = "staff_name", length = 50)
    private String staffName;

    @Column(name = "depart_id", length = 50)
    private String departId;

    @Column(name = "depart_name", length = 50)
    private String departName;

    @Column(name = "contract_type_id", length = 50)
    private String contractTypeId;

    @Column(name = "contract_type_name", length = 50)
    private String contractTypeName;

    @Column(name = "file_group_name", length = 50)
    private String fileGroupName;

    @Column(name = "file_name", length = 50)
    private String fileName;

    @Column(name = "file_origin_name", length = 50)
    private String fileOriginName;

    @Column(name = "annex_group_name", length = 50)
    private String annexGroupName;

    @Column(name = "annex_name", length = 50)
    private String annexName;

    @Column(name = "annex_origin_name", length = 50)
    private String annexOriginName;

    @Column(name = "create_time")
    private LocalDateTime createTime;

    @Column(name = "deadline")
    private LocalDateTime deadline;

}
