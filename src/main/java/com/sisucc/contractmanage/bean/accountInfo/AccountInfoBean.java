package com.sisucc.contractmanage.bean.accountInfo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author linyuqi
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tb_account_info")
public class AccountInfoBean {

    @Id
    @Column(name = "account_info_id", length = 50, nullable = false)
    private String accountInfoId;

    @Column(name = "account_code", length = 50, unique = true)
    private String accountCode;

    @Column(name = "account_password")
    private String accountPassword;

    @Column(name = "create_time")
    private LocalDateTime createTime;

    @Column(name = "update_time")
    private LocalDateTime updateTime;
}
