package com.sisucc.contractmanage.bean.mail;

/**
 * @author linyuqi
 */

public enum BodyType {

    /**
     * 邮件模版
     */
    EMAIL_TEXT_BODY("email_text_key","[企业合同管理系统] 您正在绑定个人邮箱，验证码：%s，切勿将验证码泄漏于他人，本条验证码有效期为5分钟。%s");
//    EMAIL_IMAGE_BODY("email_image_key","图片名称：%s"),
//    EMAIL_FILE_BODY("email_file_key","文件名称：%s");

    private String code ;
    private String value ;
    BodyType (String code,String value){
        this.code = code ;
        this.value = value ;
    }

    public static String getByCode (String code){
        BodyType[] values = BodyType.values() ;
        for (BodyType bodyType : values) {
            if (bodyType.code.equalsIgnoreCase(code)){
                return bodyType.value ;
            }
        }
        return null ;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
