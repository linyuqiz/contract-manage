package com.sisucc.contractmanage.bean.contractES;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * @author linyuqi
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(indexName = "tb_contract_info")
public class ContractEsBean {

    @Id
    private String contractId;

    @Field(type = FieldType.Text)
    private String contractName;

    @Field(type = FieldType.Text)
    private String contractStatus;

    @Field(type = FieldType.Text, analyzer = "ik_max_word", searchAnalyzer = "ik_max_word")
    private String contractContent;

    @Field(type = FieldType.Text)
    private String departName;

    @Field(type = FieldType.Text)
    private String contractTypeName;

    @Field(type = FieldType.Text)
    private String staffCode;

    @Field(type = FieldType.Text)
    private String staffName;

    @Field(type = FieldType.Text)
    private String departId;

    @Field(type = FieldType.Text)
    private String createTime;

}
