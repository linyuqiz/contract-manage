package com.sisucc.contractmanage.bean.assistInfo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author linyuqi
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tb_assist_info")
public class AssistInfoBean {

    @Id
    @Column(name = "assist_info_id", length = 50, nullable = false)
    private String assistInfoId;

    @Column(name = "info_code", length = 50)
    private String infoCode;

    @Column(name = "info_name", length = 50)
    private String infoName;

    /**
     * 数据类型：
     * 01 --- 部门信息
     * 02 --- 合同类型信息
     */
    @Column(name = "data_type", length = 2)
    private String dataType;

    @Column(name = "create_time")
    private LocalDateTime createTime;

}
