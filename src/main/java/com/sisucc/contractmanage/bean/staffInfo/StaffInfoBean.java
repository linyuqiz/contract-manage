package com.sisucc.contractmanage.bean.staffInfo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * @author linyuqi
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tb_staff_info")
public class StaffInfoBean {

    @Id
    @Column(name = "staff_info_id", length = 50, nullable = false)
    private String staffInfoId;

    @Column(name = "staff_code", length = 50)
    private String staffCode;

    @Column(name = "staff_name", length = 50)
    private String staffName;

    @Column(name = "staff_email", length = 50)
    private String staffEmail;

    @Column(name = "depart_id", length = 50)
    private String departId;

    @Column(name = "depart_name", length = 50)
    private String departName;

    @Column(name = "account_status", length = 2)
    private String accountStatus;

    @Column(name = "create_time")
    private LocalDateTime createTime;

    @Column(name = "update_time")
    private LocalDateTime updateTime;

}
