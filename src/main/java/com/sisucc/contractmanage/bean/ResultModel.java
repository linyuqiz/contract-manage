package com.sisucc.contractmanage.bean;

import com.sisucc.contractmanage.constant.ReturnStatusCode;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author linyuqi
 */
@Data
@AllArgsConstructor
public class ResultModel implements Serializable {
    /**
     * statusCode:返回状态码
     *  0：程序执行成功！
     *  -1：程序执行失败！
     */
    private String statusCode;
    private String resultMessage;
    private Map resultMap = new HashMap();
    private List resultList = new ArrayList();

    public ResultModel() {
        statusCode = ReturnStatusCode.SUCCESS;
        resultMessage = ReturnStatusCode.SUCCESS_MESSAGE;
    }
}
