package com.sisucc.contractmanage.constant;

/**
 * @author linyuqi
 */
public final class ReturnStatusCode {
    public static final String SUCCESS = "0";
    public static final String SUCCESS_MESSAGE = "程序执行成功！";

    public static final String ERROR = "-1";
    public static final String ERROR_MESSAGE = "程序执行失败！";

    public static final String DIS_CONNECTION = "-9";
    public static final String DIS_CONNECTION_MESSAGE = "服务器断开连接！";

    public static final String DOC_EXTRACT_ERROR = "7";
    public static final String DOC_EXTRACT_ERROR_MESSAGE = "doc文件解析失败！";

    public static final String ES_SAVE_ERROR = "8";
    public static final String ES_SAVE_ERROR_MESSAGE = "ES存储数据失败！";

    public static final String ES_QUERY_ERROR = "9";
    public static final String ES_QUERY_ERROR_MESSAGE = "ES查询数据失败！";

    public static final String ES_DELETE_ERROR = "10";
    public static final String ES_DELETE_ERROR_MESSAGE = "ES删除数据失败！";

    public static final String FASTDFS_FILE_ERROR = "11";
    public static final String FASTDFS_FILE_ERROR_MESSAGE = "FASTDFS合同文件存储失败！";

    public static final String FASTDFS_ANNEX_ERROR = "12";
    public static final String FASTDFS_ANNEX_ERROR_MESSAGE = "FASTDFS合同附件存储失败！";

    public static final String SUCCESS_CODE = "SUCCESS";
    public static final String ERROR_CODE = "ERROR";
    public static final Long CODE_HIRE_TIME_MINUTES = 5L;

}
