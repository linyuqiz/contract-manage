package com.sisucc.contractmanage.constant;

/**
 * @author linyuqi
 */
public final class ConstantDataType {
    public static final String ROOT_USER = "admin";
    public static final String DEPARTMENT = "01";
    public static final String CONTRACT_TYPE = "02";

    public static final String CONTRACT_ACTIVE_STATUS = "进行中";
    public static final String CONTRACT_WAITING_STATUS = "待处理";
    public static final String CONTRACT_PASSIVE_STATUS = "已过期";

}
