package com.sisucc.contractmanage.constant;

/**
 * @author linyuqi
 */
public final class PropertyString {
    public static final String START_TIME_SUFFIX = " 00:00:00 GMT+0800 (中国标准时间)";
    public static final String END_TIME_SUFFIX = " 23:59:59 GMT+0800 (中国标准时间)";
}
