package com.sisucc.contractmanage.dao.accountInfo;

import com.sisucc.contractmanage.bean.accountInfo.AccountInfoBean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author linyuqi
 */
@Repository
public interface AccountInfoRepository extends JpaRepository<AccountInfoBean, String>,
        JpaSpecificationExecutor<AccountInfoBean> {

    /**
     * 查询某个账号是否存在
     * @param accountCode 条件
     * @return
     */
    AccountInfoBean findByAccountCode(String accountCode);

}
