package com.sisucc.contractmanage.dao.assistInfo;

import com.sisucc.contractmanage.bean.assistInfo.AssistInfoBean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author linyuqi
 */
@Repository
public interface AssistInfoRepository extends JpaRepository<AssistInfoBean, String>,
        JpaSpecificationExecutor<AssistInfoBean> {

    /**
     * 根据类型查询所有的数据
     * @param dataType 类型
     * @return
     */
    List<AssistInfoBean> findAllByDataType(String dataType);
}
