package com.sisucc.contractmanage.dao.contractManage;

import com.sisucc.contractmanage.bean.contractInfo.ContractInfoBean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * @author linyuqi
 */
@Repository
public interface ContractInfoRepository extends JpaRepository<ContractInfoBean, String>,
        JpaSpecificationExecutor<ContractInfoBean> {

    /**
     * 统计某个用户上传合同的总量
     * @param accountCode 用户账号
     * @return
     */
    int countByAccountCode(String accountCode);

    /**
     * 根据合同状态来统计数量
     * @param status 状态
     * @return
     */
    int countByContractStatus(String status);

    /**
     * 分组统计当前用户的合同处理状态的数量
     * @param accountCode 用户账号
     * @return
     */
    @Query(value = "select contract_status, count(contract_id) num from tb_contract_info where account_code = ?1 group by contract_status", nativeQuery = true)
    List<Map<String, String>> statisticAccountByAccountCode(String accountCode);

    /**
     * 更新合同状态为已过期
     * @param status 状态
     * @param dateTime 时间
     * @return
     */
    @Query(value = "update tb_contract_info set contract_status = ?1 where deadline > ?2", nativeQuery = true)
    List<Map<String, String>> updateContractStatus(String status, LocalDateTime dateTime);

}
