package com.sisucc.contractmanage.dao.contractES;

import com.sisucc.contractmanage.bean.contractES.ContractEsBean;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * @author linyuqi
 */
@Repository
public interface ContractEsBeanRepository extends ElasticsearchRepository<ContractEsBean, String> {


}
