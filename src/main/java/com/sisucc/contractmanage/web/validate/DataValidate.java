package com.sisucc.contractmanage.web.validate;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import com.sisucc.contractmanage.bean.SendEmailModel;
import com.sisucc.contractmanage.bean.mail.EmailType;
import com.sisucc.contractmanage.constant.ReturnStatusCode;
import com.sisucc.contractmanage.service.staffSettingService.StaffSettingService;
import com.sisucc.contractmanage.serviceImpl.EmailServiceImpl.EmailServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * @author linyuqi
 */
@Log4j2
@RestController
@Api(value = "数据信息验证")
@RequestMapping("/validate")
public class DataValidate {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private StaffSettingService staffSettingService;

    @Autowired
    private EmailServiceImpl emailService;

    @GetMapping("/getCode")
    @ApiOperation(value = "获取登陆验证码", httpMethod = "GET")
    public String getCode(HttpSession session) {
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(100, 40);
        redisTemplate.opsForValue().set(session.getId(), lineCaptcha.getCode(), ReturnStatusCode.CODE_HIRE_TIME_MINUTES, TimeUnit.MINUTES);
        log.info("当前的验证码为：" + lineCaptcha.getCode());
        log.info("当前的session为：" + session.getId());
        return lineCaptcha.getImageBase64Data();
    }

    @PostMapping("/getEmailCode")
    @ApiOperation(value = "发送邮箱验证码", httpMethod = "POST")
    public String getEmailCode(String newEmail, HttpSession session) {
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(100, 40);
        try {
            redisTemplate.opsForValue().set(session.getId(), lineCaptcha.getCode(), ReturnStatusCode.CODE_HIRE_TIME_MINUTES, TimeUnit.MINUTES);
            // send email code
            SendEmailModel model = new SendEmailModel();
            model.setReceiver(newEmail);
            emailService.sendEmail(EmailType.EMAIL_TEXT_KEY.getCode(), lineCaptcha.getCode(), model);
            log.info("邮箱验证码为：" + lineCaptcha.getCode());
            log.info("当前的session为：" + session.getId());
            return ReturnStatusCode.SUCCESS_CODE;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ReturnStatusCode.ERROR_CODE;
    }

    @GetMapping("/verifyCode")
    @ApiOperation(value = "验证登陆验证码", httpMethod = "GET")
    public String verifyCode(String code, HttpSession session) {
        String str = redisTemplate.opsForValue().get(session.getId());
        if (Optional.ofNullable(code).isPresent() && Optional.ofNullable(str).isPresent()) {
            if (code.equals(str)) {
                redisTemplate.delete(session.getId());
                return ReturnStatusCode.SUCCESS_CODE;
            }
        }
        redisTemplate.delete(session.getId());
        return ReturnStatusCode.ERROR_CODE;
    }

    @GetMapping("/init")
    @ApiOperation(value = "初始化admin", httpMethod = "GET")
    public String init() {
        return staffSettingService.init();
    }

}
