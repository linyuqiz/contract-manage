package com.sisucc.contractmanage.web;

import com.spire.doc.Document;
import io.github.bluemiaomiao.service.FastdfsClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileWriter;
import java.io.IOException;

/**
 * @author linyuqi
 */
@RestController
public class DocTextController {

    @Autowired
    private FastdfsClientService remoteService;

    @RequestMapping("/getText")
    public String getText() throws IOException {
        //加载测试文档
        Document doc = new Document();
//        doc.loadFromFile("/Users/linyuqi/Desktop/李浩.开题报告.doc");
        doc.setHtmlBaseUrl("http://www.sisucc.com:8888/group1/M00/00/00/udQ6a1_vO_mAcq0VAACA0hQWh2U253.doc");

        //获取文本保存为String
        String text = doc.getText();

        //将String写入Txt
//        writeStringToTxt(text, "/Users/linyuqi/Desktop/sc.txt");
        return text;
    }

    public static void writeStringToTxt(String content, String txtFileName) throws IOException {

        FileWriter fWriter = new FileWriter(txtFileName, true);
        try {
            fWriter.write(content);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                fWriter.flush();
                fWriter.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

}
