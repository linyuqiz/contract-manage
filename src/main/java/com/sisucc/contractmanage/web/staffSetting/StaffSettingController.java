package com.sisucc.contractmanage.web.staffSetting;

import com.sisucc.contractmanage.bean.ResultModel;
import com.sisucc.contractmanage.service.staffSettingService.StaffSettingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.util.HashMap;

/**
 * @author linyuqi
 */
@Log4j2
@RestController
@Api(tags = "职员管理模块")
public class StaffSettingController {

    @Autowired
    private StaffSettingService service;

    @PostMapping("/queryStaffSettingInfo")
    @ApiOperation(value = "查询所有职员信息", httpMethod = "POST")
    public ResultModel queryStaffSettingInfo(String staffCodeParam, String staffNameParam, String startTimeParam,
                                             String endTimeParam, String accountStatusDropdownParam,
                                             String departDropdownParam, String currentPage, String pageSize) throws ParseException {
        String accountStatusDropdown = accountStatusDropdownParam, departDropdown = departDropdownParam;
        if ("-1".equals(accountStatusDropdownParam)) {
            accountStatusDropdown = "";
        }
        if ("-1".equals(departDropdownParam)) {
            departDropdown = "";
        }
        HashMap<String, Object> params = new HashMap<>();
        params.put("staffCodeParam", staffCodeParam);
        params.put("staffNameParam", staffNameParam);
        params.put("startTimeParam", startTimeParam);
        params.put("endTimeParam", endTimeParam);
        params.put("accountStatusDropdownParam", accountStatusDropdown);
        params.put("departDropdownParam", departDropdown);
        params.put("currentPage", currentPage);
        params.put("pageSize", pageSize);
        log.info("查询所有职员信息的参数：" + params.toString());
        return service.queryStaffSettingInfo(params);
    }

    @PostMapping("/saveStaffSettingInfo")
    @ApiOperation(value = "添加新的职员信息", httpMethod = "POST")
    public ResultModel saveStaffSettingInfo(String staffCode, String staffName, String departId,
                                            String loginAccount, String loginPassword, String accountStatus) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("staffCode", staffCode);
        params.put("staffName", staffName);
        params.put("departId", departId);
        params.put("loginAccount", loginAccount);
        params.put("loginPassword", loginPassword);
        params.put("accountStatus", accountStatus);
        log.info("添加新的职员信息的参数：" + params.toString());
        return service.saveStaffSettingInfo(params);
    }

    @PostMapping("/modifyStaffSettingInfo")
    @ApiOperation(value = "修改职员信息", httpMethod = "POST")
    public ResultModel modifyStaffSettingInfo(String staffInfoId, String staffCode, String staffName, String departId,
                                            String loginAccount, String loginPassword, String accountStatus) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("staffInfoId", staffInfoId);
        params.put("staffCode", staffCode);
        params.put("staffName", staffName);
        params.put("departId", departId);
        params.put("loginAccount", loginAccount);
        params.put("loginPassword", loginPassword);
        params.put("accountStatus", accountStatus);
        log.info("修改职员信息的参数：" + params.toString());
        return service.modifyStaffSettingInfo(params);
    }

    @PostMapping("/deleteStaffSettingInfo")
    @ApiOperation(value = "删除职员信息", httpMethod = "POST")
    public ResultModel deleteStaffSettingInfo(String staffSettingId) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("staffSettingId", staffSettingId);
        log.info("删除职员信息的参数：" + params.toString());
        return service.deleteStaffSettingInfo(params);
    }

    @PostMapping("/queryStaffSettingInfoById")
    @ApiOperation(value = "职员ID信息", httpMethod = "POST")
    public ResultModel queryStaffSettingInfoById(String staffSettingId) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("staffSettingId", staffSettingId);
        log.info("职员ID信息的参数：" + params.toString());
        return service.queryStaffSettingInfoById(params);
    }

    @PostMapping("/verifyLoginAccount")
    @ApiOperation(value = "职员ID信息", httpMethod = "POST")
    public ResultModel verifyLoginAccount(String loginAccount) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("loginAccount", loginAccount);
        log.info("登陆账号信息的参数：" + params.toString());
        return service.verifyLoginAccount(params);
    }
}
