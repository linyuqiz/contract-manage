package com.sisucc.contractmanage.web.myTask;

import com.sisucc.contractmanage.bean.ResultModel;
import com.sisucc.contractmanage.bean.accountInfo.AccountInfoBean;
import com.sisucc.contractmanage.service.myTaskService.MyTaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.HashMap;

/**
 * @author linyuqi
 */
@Log4j2
@RestController
@Api(tags = "部门管理模块")
public class MyTaskController {

    @Autowired
    private MyTaskService service;

    @PostMapping("/queryContractCountInfo")
    @ApiOperation(value = "统计当前用户的合同数量", httpMethod = "POST")
    public ResultModel queryContractInfo(HttpSession session) {
        AccountInfoBean accountInfo = (AccountInfoBean)session.getAttribute("currentAccountInfo");
        HashMap<String, Object> params = new HashMap<>();
        params.put("accountCode", accountInfo.getAccountCode());
        log.info("统计当前用户的合同数量：" + params.toString());
        return service.queryContractCountInfo(params);
    }
}
