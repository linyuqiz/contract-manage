package com.sisucc.contractmanage.web.login;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

/**
 * @author linyuqi
 */
@Log4j2
@Controller
public class LoginController {

    @RequestMapping("/loginPage")
    public String loginPage() {
        return "login/login";
    }

    @RequestMapping("/toLogin")
    public String toLogin(HttpSession session) {
        session.setAttribute("message", "用户名或密码错误！");
        log.info("登陆失败：用户名或密码错误！");
        return "login/login";
    }

    @RequestMapping("/toIndex")
    public String toIndex() {
        return "redirect:/index";
    }

}
