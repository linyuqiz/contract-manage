package com.sisucc.contractmanage.web;

import io.github.bluemiaomiao.service.FastdfsClientService;
import org.csource.fastdfs.FileInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;

/**
 * @author linyuqi
 */
@Controller
public class FastDfsController {

    @Autowired
    private FastdfsClientService remoteService;

    @ResponseBody
    @PostMapping("/upload")
    public String[] upload(@RequestParam("file") MultipartFile file) {
        String[] remoteInfo = null;
        try {
            remoteInfo = remoteService.autoUpload(file.getBytes(),"doc");
            FileInfo fileInfo1 = remoteService.queryFileInfo(remoteInfo[0], remoteInfo[1]);
            System.out.println("sisucc.com:8888/"+remoteInfo[0]+"/"+remoteInfo[1]);

            byte[] download = remoteService.download(remoteInfo[0], remoteInfo[1]);

            int delete = remoteService.delete(remoteInfo[0], remoteInfo[1]); //0代表删除成功

            remoteService.autoUpload(file.getBytes(),"doc");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return remoteInfo;
    }

}