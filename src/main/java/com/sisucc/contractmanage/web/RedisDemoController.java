package com.sisucc.contractmanage.web;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @author linyuqi
 */
@Log4j2
@RestController
public class RedisDemoController {

    /**
     * 适用于任何类型
     */
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    /**
     * 适用于string
     */
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @RequestMapping("/setRedisDemo")
    public String setKey(String key, String value) {
        try {
            redisTemplate.opsForValue().set(key, value, 1L, TimeUnit.MINUTES);
        } catch (Exception e) {
            e.printStackTrace();
            log.info(key + "的值存储失败！");
            return "error";
        }
        log.info(key + "的值存储成功：" + value);
        return "success";
    }

    @RequestMapping("/getRedisDemo")
    public String getKey(String key) {
        String value = redisTemplate.opsForValue().get(key);
        log.info("当前获取" + key + "的值：" + value);
        return value;

    }
}
