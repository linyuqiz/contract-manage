package com.sisucc.contractmanage.web.departSetting;

import com.sisucc.contractmanage.bean.ResultModel;
import com.sisucc.contractmanage.service.departSettingService.DepartSettingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.HashMap;

/**
 * @author linyuqi
 */
@Log4j2
@RestController
@Api(tags = "部门管理模块")
public class DepartSettingController {

    @Autowired
    private DepartSettingService service;

    @PostMapping("/queryDepartSettingInfo")
    @ApiOperation(value = "分页查询所有信息", httpMethod = "POST")
    public ResultModel queryContractInfo(String typeData, String currentPage, String pageSize, HttpSession session) {
        Object staffInfo = session.getAttribute("currentStaffInfo");
        HashMap<String, Object> params = new HashMap<>();
        params.put("typeData", typeData);
        params.put("currentPage", currentPage);
        params.put("pageSize", pageSize);
        log.info("分页查询所有信息的参数：" + params.toString());
        return service.queryDepartSettingInfo(params);
    }

    @PostMapping("/queryAllDepartSetting")
    @ApiOperation(value = "查询所有信息", httpMethod = "POST")
    public ResultModel queryAllDepartSetting(String typeData) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("typeData", typeData);
        log.info("查询信息的参数：" + params.toString());
        return service.queryAllDepartSetting(params);
    }

    @PostMapping("/saveDepartSettingInfo")
    @ApiOperation(value = "添加新的信息", httpMethod = "POST")
    public ResultModel saveDepartSettingInfo(String typeData, String departCode, String departName) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("typeData", typeData);
        params.put("departCode", departCode);
        params.put("departName", departName);
        log.info("添加新的信息的参数：" + params.toString());
        return service.saveDepartSettingInfo(params);
    }

    @PostMapping("/deleteDepartSettingInfo")
    @ApiOperation(value = "删除信息", httpMethod = "POST")
    public ResultModel deleteDepartSettingInfo(String departSettingId) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("departSettingId", departSettingId);
        log.info("删除信息的参数：" + params.toString());
        return service.deleteDepartSettingInfo(params);
    }
}
