package com.sisucc.contractmanage.web.statisticReport;

import com.sisucc.contractmanage.bean.ResultModel;
import com.sisucc.contractmanage.service.statisticReportService.StatisticReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * @author linyuqi
 */
@Log4j2
@RestController
@Api(tags = "统计报表")
public class StatisticReportController {

    @Autowired
    private StatisticReportService service;

    @PostMapping("/queryStatisticByMonth")
    @ApiOperation(value = "查询某年十二个月的合同数量", httpMethod = "POST")
    public ResultModel queryStatisticByMonth(String year) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("yearParam", year.substring(11, 15));
        log.info("查询某年十二个月的合同数量的参数：" + params.toString());
        return service.queryStatisticByMonth(params);
    }

    @PostMapping("/queryStatisticByType")
    @ApiOperation(value = "查询某年十二个月的合同数量", httpMethod = "POST")
    public ResultModel queryStatisticByType(String year) {
        HashMap<String, Object> params = new HashMap<>();
        log.info("按合同类型查询合同的合同数量的参数：" + params.toString());
        return service.queryStatisticByType(params);
    }

    @PostMapping("/queryStatisticByDepart")
    @ApiOperation(value = "按部门查询合同的合同数量", httpMethod = "POST")
    public ResultModel queryStatisticByDepart() {
        HashMap<String, Object> params = new HashMap<>();
        log.info("按部门查询合同的合同数量的参数：" + params.toString());
        return service.queryStatisticByDepart(params);
    }

}
