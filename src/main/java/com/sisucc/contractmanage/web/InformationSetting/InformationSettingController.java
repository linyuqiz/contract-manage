package com.sisucc.contractmanage.web.InformationSetting;

import com.sisucc.contractmanage.bean.accountInfo.AccountInfoBean;
import com.sisucc.contractmanage.bean.staffInfo.StaffInfoBean;
import com.sisucc.contractmanage.constant.ReturnStatusCode;
import com.sisucc.contractmanage.dao.accountInfo.AccountInfoRepository;
import com.sisucc.contractmanage.dao.staffInfo.StaffInfoRepository;
import com.sisucc.contractmanage.service.InformationSettingService.InformationSettingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * @author linyuqi
 */
@Log4j2
@RestController
@Api(tags = "信息设置")
public class InformationSettingController {

    @Autowired
    private InformationSettingService service;

    @Autowired
    private AccountInfoRepository accountInfoRepository;

    @Autowired
    private StaffInfoRepository staffInfoRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping("/getCurrentUserEmail")
    @ApiOperation(value = "获取当前用户的邮箱", httpMethod = "POST")
    public String getCurrentUserEmail(HttpSession session) {
        StaffInfoBean staffInfo = (StaffInfoBean) session.getAttribute("currentStaffInfo");
        return staffInfo.getStaffEmail();
    }

    @PostMapping("/verifyUserOldPassword")
    @ApiOperation(value = "验证用户旧的密码是否正确", httpMethod = "POST")
    public String verifyUserOldPassword(String password, HttpSession session) {
        StaffInfoBean staffInfo = (StaffInfoBean) session.getAttribute("currentStaffInfo");
        AccountInfoBean accountInfoBean = accountInfoRepository.findById(staffInfo.getStaffInfoId()).get();
        if (passwordEncoder.matches(password, accountInfoBean.getAccountPassword())) {
            return ReturnStatusCode.SUCCESS_CODE;
        }
        return ReturnStatusCode.ERROR_CODE;
    }

    @PostMapping("/updateUserPassword")
    @ApiOperation(value = "保存用户的新密码", httpMethod = "POST")
    public String updateUserPassword(String newPassword, HttpSession session) {
        StaffInfoBean staffInfo = (StaffInfoBean) session.getAttribute("currentStaffInfo");
        AccountInfoBean accountInfoBean = accountInfoRepository.findById(staffInfo.getStaffInfoId()).get();
        accountInfoBean.setAccountPassword(passwordEncoder.encode(newPassword));
        try {
            accountInfoRepository.save(accountInfoBean);
            return ReturnStatusCode.SUCCESS_CODE;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ReturnStatusCode.ERROR_CODE;
    }

    @PostMapping("/updateUserEmail")
    @ApiOperation(value = "保存用户的邮箱", httpMethod = "POST")
    public String updateUserEmail(String newEmail, HttpSession session) {
        StaffInfoBean staffInfo = (StaffInfoBean) session.getAttribute("currentStaffInfo");
        staffInfo.setStaffEmail(newEmail);
        try {
            staffInfoRepository.save(staffInfo);
            //update global user param
            session.setAttribute("currentStaffInfo", staffInfo);
            return ReturnStatusCode.SUCCESS_CODE;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ReturnStatusCode.ERROR_CODE;
    }
}
