package com.sisucc.contractmanage.web;

import com.sisucc.contractmanage.bean.SendEmailModel;
import com.sisucc.contractmanage.bean.mail.EmailType;
import com.sisucc.contractmanage.serviceImpl.EmailServiceImpl.EmailServiceImpl;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author linyuqi
 */
@Log4j2
@RestController
public class EmailController {

    @Resource
    private EmailServiceImpl emailService;

    @RequestMapping("/email")
    public String sendEmail() {
        SendEmailModel model = new SendEmailModel();
        model.setReceiver("192119741@qq.com");
        emailService.sendEmail(EmailType.EMAIL_TEXT_KEY.getCode(), "123456", model);
        log.info("执行结束====>>");
        return "success";
    }
}
