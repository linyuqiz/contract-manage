package com.sisucc.contractmanage.quartz.task;

import com.sisucc.contractmanage.dao.contractManage.ContractInfoRepository;
import com.sisucc.contractmanage.quartz.ScheduledJob;
import com.sisucc.contractmanage.serviceImpl.EmailServiceImpl.EmailServiceImpl;
import lombok.extern.log4j.Log4j2;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author linyuqi
 */
@Log4j2
@Component
public class SendPendingMessageTask extends ScheduledJob {

    @Autowired
    private ContractInfoRepository repository;

    @Autowired
    private EmailServiceImpl emailService;

    /**
     * 每天八点发送一次待处理的合同数量≠
     *
     * @return
     */
    @Override
    public String getCron() {
        return "0 0 8 * * ? *";
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        log.info("统计待处理的合同信息发送邮件通知----待处理！");

//        int count = 0;
//        try {
//            count = repository.countByContractStatus(ConstantDataType.CONTRACT_WAITING_STATUS);
//            log.info("统计待处理的合同信息发送邮件通知--待处理的合同信息为：" + count);
//        } catch (Exception e) {
//            e.printStackTrace();
//            log.info("统计待处理的合同信息发送邮件通知-------程序员行失败！");
//        }

        /**
         * TODO
         * 需要考虑将每个使用用户都发送待处理合同的消息
         */

//        try {
//            // send email code
//            SendEmailModel model = new SendEmailModel();
//            model.setReceiver(newEmail);
//            emailService.sendEmail(EmailType.EMAIL_TEXT_KEY.getCode(), String.valueOf(count), model);
//            log.info("统计待处理的合同信息发送邮件通知-------发送邮件成功！");
//        } catch (Exception e) {
//            e.printStackTrace();
//            log.info("统计待处理的合同信息发送邮件通知-------发送邮件失败！");
//        }

    }
}