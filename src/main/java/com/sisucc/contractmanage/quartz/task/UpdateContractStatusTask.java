package com.sisucc.contractmanage.quartz.task;

import com.sisucc.contractmanage.constant.ConstantDataType;
import com.sisucc.contractmanage.dao.contractManage.ContractInfoRepository;
import com.sisucc.contractmanage.quartz.ScheduledJob;
import com.sisucc.contractmanage.util.LocalDateTimeUtil;
import com.sisucc.contractmanage.util.PropertyUtil;
import lombok.extern.log4j.Log4j2;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author linyuqi
 */
@Log4j2
@Component
public class UpdateContractStatusTask extends ScheduledJob {

    @Autowired
    private ContractInfoRepository repository;

    /**
     * 每个小时更新一次合同的状态
     *
     * @return
     */
    @Override
    public String getCron() {
        return "0 0 0/1 * * ? *";
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        log.info("UpdateContractStatusTask starting：" + new Date());

        try {
            //将过期的合同修改状态修改为已过期
            repository.updateContractStatus(ConstantDataType.CONTRACT_PASSIVE_STATUS, PropertyUtil.getCurrentDateTime());
            log.info("将过期的合同修改状态修改为已过期：程序运行成功！");

            //将要过期的合同修改状态为待处理: 设置提前半个月提醒
            repository.updateContractStatus(ConstantDataType.CONTRACT_WAITING_STATUS, LocalDateTimeUtil.getLocalDateTimeStartByFlagTime(-15));
            log.info("将要过期的合同修改状态为待处理: 程序运行成功！");
        } catch (Exception e) {
            e.printStackTrace();
            log.info("将过期的合同修改状态修改为已过期或将要过期的合同修改状态为待处理：程序运行失败！");
        }

        log.info("UpdateContractStatusTask ending：" + new Date());

    }
}