package com.sisucc.contractmanage.serviceImpl.EmailServiceImpl;

import com.sisucc.contractmanage.bean.SendEmailModel;
import com.sisucc.contractmanage.bean.mail.BodyType;
import com.sisucc.contractmanage.bean.mail.EmailType;
import com.sisucc.contractmanage.util.EmailUtil;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * @author linyuqi
 */
@Component
@Service
public class EmailServiceImpl {

    @Async("taskExecutor")
    public void sendEmail(String emailKey, String code, SendEmailModel model) {
        try {
            // 异步执行
            Thread.sleep(1000);
            String textBody = EmailUtil.convertTextModel(BodyType.getByCode(emailKey), code, "");
            // 发送文本邮件
            EmailUtil.sendEmail01(model.getReceiver(), EmailType.getByCode(emailKey), textBody);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
