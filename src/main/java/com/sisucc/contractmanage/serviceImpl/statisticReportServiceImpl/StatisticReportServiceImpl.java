package com.sisucc.contractmanage.serviceImpl.statisticReportServiceImpl;

import com.sisucc.contractmanage.bean.ResultModel;
import com.sisucc.contractmanage.constant.ReturnStatusCode;
import com.sisucc.contractmanage.dao.staffInfo.StaffInfoRepository;
import com.sisucc.contractmanage.service.statisticReportService.StatisticReportService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author linyuqi
 */
@Log4j2
@Service
@Transactional
public class StatisticReportServiceImpl implements StatisticReportService {

    @Autowired
    private StaffInfoRepository repository;


    @Override
    public ResultModel queryStatisticByMonth(Map<String, Object> params) {
        ResultModel model = new ResultModel();
        String yearParam = (String) params.get("yearParam");
        String yearInit = yearParam + "-01-01 00:00:00";
        String yearEnd = yearParam + "-12-31 23:59:59";
        try {
            log.info("查询某年十二个月的合同数量的参数：" + yearParam);
            List<Map<String, String>> mapList = repository.statisticReportByYear(yearInit, yearEnd);
            model.setResultList(mapList);
        } catch (Exception e) {
            e.printStackTrace();
            model.setStatusCode(ReturnStatusCode.ERROR);
            model.setResultMessage(ReturnStatusCode.ERROR_MESSAGE);
            log.info("查询某年十二个月的合同数量---程序执行失败！");
        }
        return model;
    }

    @Override
    public ResultModel queryStatisticByType(Map<String, Object> params) {
        ResultModel model = new ResultModel();
        try {
            List<Map<String, String>> mapList = repository.statisticReportByType();
            model.setResultList(mapList);
        } catch (Exception e) {
            e.printStackTrace();
            model.setStatusCode(ReturnStatusCode.ERROR);
            model.setResultMessage(ReturnStatusCode.ERROR_MESSAGE);
            log.info("按合同类型查询合同的合同数量---程序执行失败！");
        }
        return model;
    }

    @Override
    public ResultModel queryStatisticByDepart(Map<String, Object> params) {
        ResultModel model = new ResultModel();
        try {
            List<Map<String, String>> mapList = repository.statisticReportByDepart();
            model.setResultList(mapList);
        } catch (Exception e) {
            e.printStackTrace();
            model.setStatusCode(ReturnStatusCode.ERROR);
            model.setResultMessage(ReturnStatusCode.ERROR_MESSAGE);
            log.info("按部门查询合同的合同数量---程序执行失败！");
        }
        return model;
    }
}
