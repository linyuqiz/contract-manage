package com.sisucc.contractmanage.serviceImpl.myTaskServiceImpl;

import com.sisucc.contractmanage.bean.ResultModel;
import com.sisucc.contractmanage.constant.ReturnStatusCode;
import com.sisucc.contractmanage.dao.contractManage.ContractInfoRepository;
import com.sisucc.contractmanage.service.myTaskService.MyTaskService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author linyuqi
 */
@Log4j2
@Service
@Transactional
public class MyTaskServiceImpl implements MyTaskService {

    @Autowired
    private ContractInfoRepository repository;

    @Override
    public ResultModel queryContractCountInfo(Map<String, Object> params) {
        ResultModel model = new ResultModel();
        String accountCode = (String) params.get("accountCode");
        try {
            log.info("统计当前用户的合同数量的参数：" + accountCode);
            int account = repository.countByAccountCode(accountCode);
            List<Map<String, String>> maps = repository.statisticAccountByAccountCode(accountCode);
            String processing = "0", pending = "0", expired = "0";
            for (int i = 0; i < maps.size(); i++) {
                String contract_status = maps.get(i).get("contract_status");
                String num = String.valueOf(maps.get(i).get("num"));
                if ("进行中".equals(contract_status)) {
                    processing = num;
                }
                if ("待处理".equals(contract_status)) {
                    pending = num;
                }
                if ("已过期".equals(contract_status)) {
                    expired = num;
                }
            }
            model.getResultMap().put("account", account);
            model.getResultMap().put("processing", processing);
            model.getResultMap().put("pending", pending);
            model.getResultMap().put("expired", expired);
        } catch (Exception e) {
            e.printStackTrace();
            model.setStatusCode(ReturnStatusCode.ERROR);
            model.setResultMessage(ReturnStatusCode.ERROR_MESSAGE);
            log.info("统计当前用户的合同数量---程序执行失败！");
        }
        return model;
    }
}
