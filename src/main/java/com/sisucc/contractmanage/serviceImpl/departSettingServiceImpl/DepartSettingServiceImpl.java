package com.sisucc.contractmanage.serviceImpl.departSettingServiceImpl;

import com.sisucc.contractmanage.bean.ResultModel;
import com.sisucc.contractmanage.bean.assistInfo.AssistInfoBean;
import com.sisucc.contractmanage.constant.ConstantDataType;
import com.sisucc.contractmanage.constant.ReturnStatusCode;
import com.sisucc.contractmanage.dao.assistInfo.AssistInfoRepository;
import com.sisucc.contractmanage.service.departSettingService.DepartSettingService;
import com.sisucc.contractmanage.util.PropertyUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author linyuqi
 */
@Log4j2
@Service
@Transactional
public class DepartSettingServiceImpl implements DepartSettingService {

    @Autowired
    private AssistInfoRepository repository;

    @Override
    public ResultModel queryDepartSettingInfo(Map<String, Object> params) {
        ResultModel model = new ResultModel();
        String typeData = params.get("typeData").toString();
        String dataType = "";
        if ("部门".equals(typeData)) {
            dataType = ConstantDataType.DEPARTMENT;
        }
        if ("类型".equals(typeData)) {
            dataType = ConstantDataType.CONTRACT_TYPE;
        }
        int currentPage = Integer.parseInt(params.get("currentPage").toString());
        int pageSize = Integer.parseInt(params.get("pageSize").toString());
        try {
            String finalDataType = dataType;
            Specification<AssistInfoBean> specification =
                    (Specification<AssistInfoBean>) (root, criteriaQuery, criteriaBuilder) -> {
                        List<Predicate> predicates = new ArrayList<>();
                        predicates.add(criteriaBuilder.equal(root.get("dataType"), finalDataType));
                        Predicate[] array = new Predicate[predicates.size()];
                        return criteriaBuilder.and(predicates.toArray(array));
                    };
            Page<AssistInfoBean> systemSettingBeanPage =
                    repository.findAll(specification,
                            PageRequest.of(currentPage, pageSize,
                                    Sort.by(Sort.Direction.DESC, "createTime")));
            model.getResultMap().put("total", systemSettingBeanPage.getTotalElements());
            model.getResultMap().put("pages", systemSettingBeanPage.getTotalPages());
            model.setResultList(systemSettingBeanPage.getContent());
            log.info("分页查询所有信息---列表：" + systemSettingBeanPage.getContent().toString());
            log.info("分页查询所有信息---总数：" + systemSettingBeanPage.getTotalElements());
            log.info("分页查询所有信息---页数：" + systemSettingBeanPage.getTotalPages());
        } catch (Exception e) {
            e.printStackTrace();
            model.setStatusCode(ReturnStatusCode.ERROR);
            model.setResultMessage(ReturnStatusCode.ERROR_MESSAGE);
            log.info("分页查询所有信息---程序执行失败！");
        }
        return model;
    }

    @Override
    public ResultModel queryAllDepartSetting(Map<String, Object> params) {
        ResultModel model = new ResultModel();
        String typeData = params.get("typeData").toString();
        String dataType = "";
        if ("部门".equals(typeData)) {
            dataType = ConstantDataType.DEPARTMENT;
        }
        if ("类型".equals(typeData)) {
            dataType = ConstantDataType.CONTRACT_TYPE;
        }
        try {
            List<AssistInfoBean> assistInfoBeanList = repository.findAllByDataType(dataType);
            model.setResultList(assistInfoBeanList);
            log.info("查询所有信息---列表：" + assistInfoBeanList.toString());
        } catch (Exception e) {
            e.printStackTrace();
            model.setStatusCode(ReturnStatusCode.ERROR);
            model.setResultMessage(ReturnStatusCode.ERROR_MESSAGE);
            log.info("查询所有信息---程序执行失败！");
        }
        return model;
    }

    @Override
    public ResultModel saveDepartSettingInfo(Map<String, Object> params) {
        ResultModel model = new ResultModel();
        String uuid = PropertyUtil.getUUID();
        String typeData = params.get("typeData").toString();
        String departCode = (String) params.get("departCode");
        String departName = (String) params.get("departName");
        String dataType = "";
        if ("部门".equals(typeData)) {
            dataType = ConstantDataType.DEPARTMENT;
        }
        if ("类型".equals(typeData)) {
            dataType = ConstantDataType.CONTRACT_TYPE;
        }
        LocalDateTime currentDateTime = PropertyUtil.getCurrentDateTime();
        try {
            AssistInfoBean departSettingInfo = new AssistInfoBean(uuid, departCode, departName, dataType, currentDateTime);
            log.info("添加新的信息" + departCode.toString());
            repository.save(departSettingInfo);
        } catch (Exception e) {
            e.printStackTrace();
            model.setStatusCode(ReturnStatusCode.ERROR);
            model.setResultMessage(ReturnStatusCode.ERROR_MESSAGE);
            log.info("添加新的信息---程序执行失败！");
        }
        return model;
    }

    @Override
    public ResultModel deleteDepartSettingInfo(Map<String, Object> params) {
        ResultModel model = new ResultModel();
        String departSettingId = (String) params.get("departSettingId");
        try {
            /**
             * to-do 判断当前是否还有人
             */
            log.info("删除信息" + departSettingId);
            repository.deleteById(departSettingId);
        } catch (Exception e) {
            e.printStackTrace();
            model.setStatusCode(ReturnStatusCode.ERROR);
            model.setResultMessage(ReturnStatusCode.ERROR_MESSAGE);
            log.info("删除信息---程序执行失败！");
        }
        return model;
    }
}
