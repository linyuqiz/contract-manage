package com.sisucc.contractmanage.serviceImpl.indexServiceImpl;

import com.sisucc.contractmanage.service.indexService.IndexService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author linyuqi
 */
@Log4j2
@Service
@Transactional
public class IndexServiceImpl implements IndexService {

}
