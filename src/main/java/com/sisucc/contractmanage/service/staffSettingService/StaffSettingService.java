package com.sisucc.contractmanage.service.staffSettingService;

import com.sisucc.contractmanage.bean.ResultModel;

import java.text.ParseException;
import java.util.Map;

/**
 * @author linyuqi
 */
public interface StaffSettingService {

    /**
     * 查询所有职员信息
     * @param params 条件
     * @return
     * @throws ParseException
     */
    ResultModel queryStaffSettingInfo(Map<String, Object> params) throws ParseException;

    /**
     * 添加新的职员信息
     *
     * @param params 条件
     * @return
     */
    ResultModel saveStaffSettingInfo(Map<String, Object> params);

    /**
     * 修改职员信息
     *
     * @param params 条件
     * @return
     */
    ResultModel modifyStaffSettingInfo(Map<String, Object> params);

    /**
     * 删除职员信息
     *
     * @param params 条件
     * @return
     */
    ResultModel deleteStaffSettingInfo(Map<String, Object> params);

    /**
     * 通过ID查询职员信息
     *
     * @param params 条件
     * @return
     */
    ResultModel queryStaffSettingInfoById(Map<String, Object> params);

    /**
     * 查看当前登陆账号是否已经存在
     *
     * @param params 条件
     * @return
     */
    ResultModel verifyLoginAccount(Map<String, Object> params);

    /**
     * 初始化admin账户
     * @return
     */
    String init();

}
