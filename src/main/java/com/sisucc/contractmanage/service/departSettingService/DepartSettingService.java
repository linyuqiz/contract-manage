package com.sisucc.contractmanage.service.departSettingService;

import com.sisucc.contractmanage.bean.ResultModel;

import java.util.Map;

/**
 * @author linyuqi
 */
public interface DepartSettingService {

    /**
     * 分页查询所有信息
     * @param params 条件
     * @return
     */
    ResultModel queryDepartSettingInfo(Map<String, Object> params);

    /**
     * 查询所有信息
     * @param params 条件
     * @return
     */
    ResultModel queryAllDepartSetting(Map<String, Object> params);

    /**
     * 添加新的信息
     * @param params 条件
     * @return
     */
    ResultModel saveDepartSettingInfo(Map<String, Object> params);

    /**
     * 删除信息
     * @param params 条件
     * @return
     */
    ResultModel deleteDepartSettingInfo(Map<String, Object> params);

}
