package com.sisucc.contractmanage.service.contractManageService;

import com.sisucc.contractmanage.bean.ResultModel;

import java.text.ParseException;
import java.util.Map;

/**
 * @author linyuqi
 */
public interface ContractManageService {

    /**
     * 根据条件查询符合条件的合同信息
     * @param params 查询条件
     * @return
     */
    ResultModel queryContractInfo(Map<String, Object> params) throws ParseException;

    /**
     * 保存合同信息
     * @param params 条件
     * @return
     */
    ResultModel saveContractInfo(Map<String, Object> params) throws ParseException;

    /**
     * 删除合同信息
     * @param params 条件
     * @return
     */
    ResultModel deleteContractInfo(Map<String, Object> params);

}
