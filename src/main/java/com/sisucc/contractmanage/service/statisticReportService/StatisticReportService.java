package com.sisucc.contractmanage.service.statisticReportService;

import com.sisucc.contractmanage.bean.ResultModel;

import java.util.Map;

/**
 * @author linyuqi
 */
public interface StatisticReportService {

    /**
     * 查询某年十二个月的合同数量
     * @param params 年
     * @return
     */
    ResultModel queryStatisticByMonth(Map<String, Object> params);

    /**
     * 按合同类型查询合同的合同数量
     * @return
     */
    ResultModel queryStatisticByType(Map<String, Object> params);

    /**
     * 按部门查询合同的合同数量
     * @return
     */
    ResultModel queryStatisticByDepart(Map<String, Object> params);

}
