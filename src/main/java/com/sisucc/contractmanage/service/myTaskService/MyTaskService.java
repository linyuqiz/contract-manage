package com.sisucc.contractmanage.service.myTaskService;

import com.sisucc.contractmanage.bean.ResultModel;

import java.util.Map;

/**
 * @author linyuqi
 */
public interface MyTaskService {

    /**
     * 统计当前的用户的合同数据
     * @param params 条件
     * @return
     */
    ResultModel queryContractCountInfo(Map<String, Object> params);

}
