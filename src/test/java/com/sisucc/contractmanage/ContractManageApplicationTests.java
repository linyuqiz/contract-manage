package com.sisucc.contractmanage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;

@SpringBootTest
class ContractManageApplicationTests {

    //自动注入即可使用
    @Autowired
    private ElasticsearchRestTemplate esRestTemplate;

    /*//按id查询
    @Test
    void save() {
        esRestTemplate.save(new UserEs("111", "java开发", "45.90", "java"));
    }

    //按id查询
    @Test
    void testQueryBookById() {
        UserEs book = esRestTemplate.get("111", UserEs.class);
        System.out.println(book.toString());
    }*/

}
