# 企业合同管理项目(毕设)

## 项目概述
本系统利用Java Web技术、文件存储技术、Email发送技术和数据库存储技术等，开发了基于Spring Boot、Spring Data JPA、Fast DFS和MySQL等技术的B/S架构的企业合同管理系统。

该系统操作简单、界面新颖，将会实现对合同信息的增删改查和下载，还有对合同内容信息的全文检索功能，历史合同信息的按月统计和按部门统计的数据可视化功能等，可以根据多条件进行合同查询，根据内容进行检索，可以查看历年来合同的统计分析数据图表等。

## 项目部署
将该系统运行在CentOS 8.2的环境下来模拟真实生产环境中可能遇到的一下问题，这样测试的结果将更加的准确，该系统将会打包成Docker镜像发布在服务器上通过容器技术进行部署，详情如下：  
硬件环境：阿里云云服务器 2H4G  
系统环境：CentOS 8.2  
服务部署：Docker 部署Fast DFS、ES、Redis和MySQL以及该系统  
浏览器：Google Chrome  

### 部署流程
1. 在服务器上通过Docker部署Fast DFS、ES、Redis和MySQL等服务  
详细的配置流程参考博客：https://blog.csdn.net/qq_40307379/article/details/113776631?spm=1001.2014.3001.5501

2. 获取该项目代码：下载源码 & git拉取，建议使用Idea开发环境

3. 项目账户初始化，调用指定接口：http://localhost:8080/validate/init  
该项目没有数据库sql脚本，一切使用spring data JPA通过模型去反向生成数据表，但是需要提前创建好数据库 db_contract_manage  
初始化账户：admin  初始化密码：123
   
4. [可选]docker部署该系统于远程服务器
该部分根据情况选择，目前采用的是项目单独部署，具体的部署流程参考博客：https://blog.csdn.net/qq_40307379/article/details/112858330?spm=1001.2014.3001.5501
   
## 实现效果
### 登陆页面
![img.png](image/login-page.png)

### 合同管理页面
![img.png](image/contract-page.png)

### 我的任务
![img.png](image/mytask-page.png)

### 统计图表
![img.png](image/statistic-page.png)

## 项目总结
该项目只是用于毕设的基本功能演示，例如页面细节、全新啊设置、定时任务的注入等需要进一步完善。

## 联系方式
如有需要，email：192119741@qq.com

注意：个人毕设项目，谨慎使用！



